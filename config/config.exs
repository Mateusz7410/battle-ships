# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :battle_ships,
  ecto_repos: [BattleShips.Repo]

# Configures the endpoint
config :battle_ships, BattleShipsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yYkxs13D/sLHdqwRh7foGobR8qPYb5CGSqYHj1GoC1gt2LWJLrevz9cSUGu91+Au",
  render_errors: [view: BattleShipsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BattleShips.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
