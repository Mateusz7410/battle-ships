// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
import {connectToSocket, logout, invitePlayer, getMousePos} from "./socket"

$(document).ready(function() {
  $("#playButton").click(function() {
    var name = $("#name").val()
    if (name) {
      connectToSocket(name.trim(), document)
    } else {
      alert("You should enter your Name to join the game")
    }
  })
  $("#logOutButton").click(function() {
    logout()
  })
  $("#invitePlayerButton").click(function(){
    invitePlayer()
  })
  $("#canvas2").click(function(evt){
    getMousePos(evt)
  })
})
