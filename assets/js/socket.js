// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket,
// and connect at the socket path in "lib/web/endpoint.ex".
//
// Pass the token on params as below. Or remove it
// from the params if you are not using authentication.
import {Socket} from "./my_phoenix"
import binarySocket from "./binarySocket"
let me
let players = []
let token
let playersContainer = document.getElementById("players")
let playGame = document.getElementById("playGame")
let logoutGame = document.getElementById("logoutGame")
let playSection = document.getElementById("playSection")
let myBoard = document.getElementById("canvas1")
let opponentBoard = document.getElementById("canvas2")
let inviteSection = document.getElementById("inviteSection")
let turn = document.getElementById("turn")
let turnLabel = document.getElementById("turnLabel")
let playerNameLabel = document.getElementById("playerNameLabel")
let socket
let channel
logoutGame.style.visibility = "hidden"
playSection.style.visibility = "hidden"
var msgpack = require("msgpack-lite");

if(localStorage.getItem("storedName")){
  localStorage.setItem("refreshFlag", false)
  console.log('refreshed, local');
  me = localStorage.getItem("storedName")
  playerNameLabel.innerHTML = "My Name: " + me
  token = localStorage.getItem("storedToken")
  socket = new Socket("/socket", {params: {name: me, type: "msgpack", token: token}})
  socket = binarySocket.convertToBinary(socket);
  socket.onError(error => {
    alert("This name is taken. Choose another one!")
    socket.disconnect()
  });
  socket.connect()
  channel = socket.channel("player:lobby", {})

  // joins the channel
  channel.join()
  .receive("ok", params => { // on joining channel, we receive the current players list
    if(localStorage.getItem("refreshFlag") == "false") {
      console.log('Joined to channel after refresh');
      setupChannelMessageHandlers(channel)
      players = params.players
      showPlayers()
      playGame.style.display = "none"
      turn.style.display = "none"
      logoutGame.style.visibility = "visible"
      playSection.style.visibility = "visible"
      console.log(params.fields1);
      drawBoard(params.fields1, params.fields2)
      localStorage.setItem("refreshFlag", true)

      if(localStorage.getItem("isPlaying") == "true") {
        inviteSection.style.display = "none"
        turn.style.display = "block"
        turnLabel.innerHTML = "Current player turn: " + localStorage.getItem("actualPlayer")
      }
    }

  })

  //TODO rysowanie plansz
}

function drawBoard(fields1, fields2) {
  if(fields1 == null) {
    fields1 = "                         "
    fields2 = "                         "
  }
  var ctx = myBoard.getContext("2d");
  for(var i = 0;i<5;i++){
    for(var j = 0;j<5;j++){
      if(fields1.charAt(i*5+j) == " ") {
        ctx.fillStyle = "#C0C0C0"
      }else if(fields1.charAt(i*5+j) == "O"){
        ctx.fillStyle = "#000000"
      }else if(fields1.charAt(i*5+j) == "X"){
        ctx.fillStyle = "#FF0000"
      }else if(fields1.charAt(i*5+j) == "."){
        ctx.fillStyle = "#00FFFF"
      }else if(fields1.charAt(i*5+j) == "K"){
        ctx.fillStyle = "#000000"
      }

      ctx.fillRect(i * 60 + 5,j*60 + 5,50,50);
    }
  }

  var ctx = opponentBoard.getContext("2d");
  for(var i = 0;i<5;i++){
    for(var j = 0;j<5;j++){
      if(fields2.charAt(i*5+j) == " ") {
        ctx.fillStyle = "#C0C0C0"
      }else if(fields2.charAt(i*5+j) == "O"){
        ctx.fillStyle = "#000000"
      }else if(fields2.charAt(i*5+j) == "X"){
        ctx.fillStyle = "#FF0000"
      }else if(fields2.charAt(i*5+j) == "."){
        ctx.fillStyle = "#00FFFF"
      }else if(fields2.charAt(i*5+j) == "K"){
        ctx.fillStyle = "#000000"
      }
      ctx.fillRect(i * 60 + 5,j*60 + 5,50,50);
    }
  }
}


function mapElements(value, key, map) {
  if(value != me && checkIfAdd(value)) {
    var opt = document.createElement('option');
    opt.appendChild( document.createTextNode(value) );
    opt.value = value;
    playersContainer.appendChild(opt);
  }
}

function showPlayers() {
	players.forEach(mapElements)
}

function checkIfAdd(name) {
  for (var i=0; i<playersContainer.length; i++){
  if (playersContainer.options[i].value == name )
     return false
  }
  return true
}

function setupChannelMessageHandlers(channel) {
  // New player joined the game
  channel.on("player:joined", ({name: name}) => {
  	if(name != me && checkIfAdd(name)){
      var opt = document.createElement('option');
      opt.appendChild( document.createTextNode(name) );
      opt.value = name;
      playersContainer.appendChild(opt);
	    players.push(name)
  	}
  })

  channel.on("player:disconnected", ({name: name}) => {
    if(localStorage.getItem("opponent") == name && localStorage.getItem("isPlaying") == "true") {
      alert("Player disconnected... :(")
      localStorage.removeItem("isPlaying");
      localStorage.setItem("isPlaying", false)
      localStorage.removeItem("opponent")
      localStorage.removeItem("actualPlayer")
      inviteSection.style.display = "block"
      turn.style.display = "none"
      drawBoard(null, null)
    }
    for (var i=0; i<playersContainer.length; i++){
    if (playersContainer.options[i].value == name )
       playersContainer.remove(i);
    }
  })

  channel.on("player:invited", ({response: response}) => {
    if(response == "error"){
      alert("This player is busy! Choose another one!")
    }
  })

  channel.on("player:inviting", ({name: name}) => {
    if (confirm('Player ' + name + ' invited you to game! Press Ok to accept, Cancel to reject invitation.')) {
      localStorage.setItem("isPlaying", true)
      localStorage.setItem("opponent", name)
      localStorage.setItem("actualPlayer", name)
      inviteSection.style.display = "none"
      turn.style.display = "block"
      turnLabel.innerHTML = "Current player turn: " + name
      channel.push("player:accept", {playerName: name})
    } else {
        channel.push("player:reject", {playerName: name})
    }
  })

  channel.on("player:board_refresh", ({boards: boards}) => {
    drawBoard(boards.fields1, boards.fields2)
  })


  channel.on("player:accept", ({name: name}) => {
    localStorage.setItem("isPlaying", true)
    localStorage.setItem("opponent", name)
    localStorage.setItem("actualPlayer", me)
    inviteSection.style.display = "none"
    turn.style.display = "block"
    turnLabel.innerHTML = "Current player turn: " + me
    console.log('player accepted invitation')
  })

  channel.on("player:reject", ({name: name}) => {
    console.log('player rejected invitation')
  })

  channel.on("player:boards", ({name: name, fields1: fields1, fields2: fields2}) => {
    drawBoard(fields1, fields2)
  })

  channel.on("player:move", ({fields1: fields1, fields2: fields2, player2: player2, hit: hit}) => {
    if(hit != true) {
      if(localStorage.getItem("actualPlayer") == me) {
        localStorage.setItem("actualPlayer", player2)
        turnLabel.innerHTML = "Current player turn: " + localStorage.getItem("actualPlayer")
      } else {
        localStorage.setItem("actualPlayer", me)
        turnLabel.innerHTML = "Current player turn: " + localStorage.getItem("actualPlayer")
      }
    }
    drawBoard(fields1, fields2)
  })

  channel.on("player:finished", ({winner: winner}) => {
    console.debug(winner)
    if(winner) {
      alert("You win!!!!! Congratulations!! :D")
    } else {
      alert("You lose... Better luck next time :)")
    }
    localStorage.removeItem("isPlaying");
    localStorage.setItem("isPlaying", false)
    localStorage.removeItem("opponent")
    localStorage.removeItem("actualPlayer")
    inviteSection.style.display = "block"
    turn.style.display = "none"
    drawBoard(null, null)
  })
}

function connectToSocket(name, document) {
  // connects to the socket endpoint
  console.log('socket main connect');

  if (localStorage.getItem("storedToken")) {
    token = localStorage.getItem("storedToken")
  }
  else {
    token = "noToken"
  }
  console.debug(token)
  socket = new Socket("/socket", {params: {name: name, type: "msgpack", token: token}})
  socket = binarySocket.convertToBinary(socket);
  socket.onError(error => {
    alert("This name is taken. Choose another one!")
    socket.disconnect()
  });
  localStorage.setItem("url", window.location.href);
  socket.connect()
  channel = socket.channel("player:lobby", {})
  let gameChannel
  me = name
  playerNameLabel.innerHTML = "My Name: " + me

  // joins the channel
  channel.join()
  .receive("ok", params => { // on joining channel, we receive the current players list
    console.debug(socket)
    if(localStorage.getItem("storedName") == null) {
      console.log('Joined to channel');
      if (params.token != "nothing") {
        token = params.token
        localStorage.setItem("storedToken", token);
      }

      localStorage.setItem("storedName", name);
      localStorage.setItem("isPlaying", false);
      window.location.replace(localStorage.getItem("url"));
    }
  })
}

function getMousePos(evt) {
  if(localStorage.getItem("isPlaying") == "true") {
    if(localStorage.getItem("actualPlayer") == me) {
      var rect = opponentBoard.getBoundingClientRect();
      var x = Math.floor((evt.clientX - rect.left) / 60)
      var y = Math.floor((evt.clientY - rect.top) / 60)
      channel.push("player:move", {x: x, y: y})
    } else {
      alert("It is not your turn!")
    }
  } else {
    alert("You must invite player first!")
  }
}

function logout() {
	playGame.style.display = "block"
	logoutGame.style.visibility = "hidden"
  playSection.style.visibility = "hidden"
  channel.push("player:disconnect", {name: me})
  socket.disconnect()
  console.debug(localStorage.getItem("url"))
  window.location.replace(localStorage.getItem("url"));
  localStorage.removeItem("storedName");
  localStorage.removeItem("storedToken")
}

function invitePlayer() {
  var playerName = playersContainer.options[playersContainer.selectedIndex].text
  channel.push("player:invite", {playerName: playerName})
}

export {connectToSocket, logout, invitePlayer, getMousePos}
