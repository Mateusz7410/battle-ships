defmodule BattleShipsWeb.PageController do
  use BattleShipsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
