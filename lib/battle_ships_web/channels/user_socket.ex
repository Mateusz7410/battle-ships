defmodule BattleShipsWeb.UserSocket do
  use Phoenix.Socket

  ## Channels
  channel "player:*", BattleShipsWeb.PlayerChannel

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  #def connect(%{"name" => name, "token" => token}, socket, _connect_info) do
  #  IO.inspect('Witam')
  #  IO.inspect(token)
  #  case name do
  #    nil ->
  #      :error
  #    "" ->
  #      :error
  #    name ->
  #      case BattleShips.GameAgent.get_player(name) do
  #        nil ->
  #          :error
  #        player ->
  #          case Phoenix.Token.verify(BattleShipsWeb.Endpoint, "user salt", token) do
  #            {:ok, player_id} ->
  #              if BattleShips.GameAgent.get_player(name).id == player_id do
  #                socket = assign(socket, :name, name)
  #                socket = assign(socket, :token, token)
  #                socket = assign(socket, :reconnect, "reconnect")
  #                {:ok, socket}
  #              else
  #                :error
  #              end
  #            {:error, _params} ->
  #              :error
  #          end
  #      end
  #  end
  #end


  def connect(%{"name" => name, "token" => token}, socket, _connect_info) do
    IO.inspect(name)
    IO.inspect(token)
    IO.inspect(Phoenix.Token.verify(BattleShipsWeb.Endpoint, "user salt", token))
    case name do
      nil ->
        :error
      "" ->
        :error
      name ->
        case BattleShips.GameAgent.get_player(name) do
          nil ->
            {:ok, player} =
            BattleShips.Player.create(%{name: name})
            token = Phoenix.Token.sign(BattleShipsWeb.Endpoint, "user salt", player.id)
            socket = assign(socket, :name, name)
            socket = assign(socket, :token, token)
            BattleShips.GameAgent.put_player(%{token: token, socket: socket, name: name, id: player.id, is_invited: nil, is_inviting: nil, fields1: nil, fields2: nil, player2: nil})
            {:ok, socket}
          player ->
            case Phoenix.Token.verify(BattleShipsWeb.Endpoint, "user salt", token) do
              {:ok, player_id} ->
                if player.id == player_id do
                  socket = assign(socket, :name, name)
                  socket = assign(socket, :token, "nothing")
                  {:ok, socket}
                else
                  :error
                end
              {:error, _params} ->
                :error
            end
        end
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     BattleShipsWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
end
