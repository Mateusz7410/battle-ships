defmodule BattleShipsWeb.PlayerChannel do
  use Phoenix.Channel

  def join("player:lobby", params, socket) do
    token =
    if socket.assigns.token != "nothing" do
      socket.assigns.token
    else
      "nothing"
    end

    if(!Map.has_key?(socket.assigns, :reconnect)) do
      send(self, {:after_join, params})
    end
  	players = BattleShips.PlayerFunctions.get_players()
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    BattleShips.GameAgent.update_player(%{player | socket: socket})
    {:ok, %{token: token, players: players, fields1: player.fields1, fields2: player.fields2},  socket}
  end

  def handle_info({:after_join, _message}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    BattleShips.GameAgent.update_player(%{player | socket: socket})
    name = socket.assigns.name
    broadcast! socket, "player:joined", %{name: name}
    {:noreply, socket}
  end

  def handle_in("player:invite", %{"playerName" => playerName}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    BattleShips.GameAgent.update_player(%{player | socket: socket})
    invitedPlayer = BattleShips.GameAgent.get_player(playerName)
    response =
    if invitedPlayer.is_inviting == nil and invitedPlayer.is_invited == nil do
      BattleShips.GameAgent.update_player(%{invitedPlayer | is_invited: player.name})
      BattleShips.GameAgent.update_player(%{player | is_inviting: invitedPlayer.name})
      push(invitedPlayer.socket, "player:inviting", %{name: player.name})
      %{response: "ok"}
    else
      %{response: "error"}
    end
    push(socket, "player:invited", response)
    {:noreply, socket}
  end

  def handle_in("player:accept", %{"playerName" => playerName}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    BattleShips.GameAgent.update_player(%{player | socket: socket})
    invitingPlayer = BattleShips.GameAgent.get_player(playerName)

    invitingPlayer = BattleShips.GameAgent.update_player(%{invitingPlayer | fields1: starting_fields(), fields2: empty_fields(), player2: player.name})
    player = BattleShips.GameAgent.update_player(%{player | fields1: starting_fields(), fields2: empty_fields(), player2: invitingPlayer.name})
    push(invitingPlayer.socket, "player:accept", %{name: player.name})
    push(invitingPlayer.socket, "player:boards", %{name: player.name, fields1: invitingPlayer.fields1, fields2: invitingPlayer.fields2})
    push(socket, "player:boards", %{name: invitingPlayer.name, fields1: player.fields1, fields2: player.fields2})
    {:noreply, socket}
  end

  def handle_in("player:reject", %{"playerName" => playerName}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    BattleShips.GameAgent.update_player(%{player | socket: socket})
    invitingPlayer = BattleShips.GameAgent.get_player(playerName)

    BattleShips.GameAgent.update_player(%{invitingPlayer | is_inviting: nil})
    BattleShips.GameAgent.update_player(%{player | is_invited: nil})
    push(invitingPlayer.socket, "player:reject", %{name: player.name})
    {:noreply, socket}
  end

  def handle_in("player:disconnect", %{"name" => name}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    if player.player2 != nil do
      player2 = BattleShips.GameAgent.get_player(player.player2)
      BattleShips.GameAgent.update_player(%{player2 |  fields1: nil, fields2: nil, player2: nil, is_invited: nil, is_inviting: nil})
    end
    BattleShips.GameAgent.delete_player(socket.assigns.name)
    broadcast! socket, "player:disconnected", %{name: name}
    {:noreply, socket}
  end

  def handle_in("player:move", %{"x" => x, "y" => y}, socket) do
    player = BattleShips.GameAgent.get_player(socket.assigns.name)
    player2 = BattleShips.GameAgent.get_player(player.player2)

    hit =
      if String.at(player2.fields1, x * 5 + y) == "X" do
        true
      else
        false
      end

    player =
    case String.at(player2.fields1, x * 5 + y) do
      "X" ->
        %{player | fields2: update_fields(player.fields2, "K", x * 5 + y)}
      " " ->
        %{player | fields2: update_fields(player.fields2, ".", x * 5 + y)}
      _ ->
        player
    end

    player2 =
    case String.at(player2.fields1, x * 5 + y) do
      "X" ->
        %{player2 | fields1: update_fields(player2.fields1, "K", x * 5 + y)}
      " " ->
        %{player2 | fields1: update_fields(player2.fields1, ".", x * 5 + y)}
      _ ->
        player2
    end

    if check_win_conditions(player.fields2) do
      push(socket, "player:finished", %{winner: true})
      push(player2.socket, "player:finished", %{winner: false})
      BattleShips.GameAgent.update_player(%{player |  socket: socket, fields1: nil, fields2: nil, player2: nil, is_invited: nil, is_inviting: nil})
      BattleShips.GameAgent.update_player(%{player2 |  fields1: nil, fields2: nil, player2: nil, is_invited: nil, is_inviting: nil})
      {:noreply, socket}
    else
      BattleShips.GameAgent.update_player(%{player |  socket: socket})
      BattleShips.GameAgent.update_player(player2)
      push(socket, "player:move", %{fields1: player.fields1, fields2: player.fields2, player2: player.player2, hit: hit})
      push(player2.socket, "player:move", %{fields1: player2.fields1, fields2: player2.fields2, player2: player2.player2, hit: hit})
      {:noreply, socket}
    end
  end

  defp update_fields([] = fields, str, index, result) when is_list(fields) do
    result
    |> Enum.reverse()
    |> List.to_string()
  end

  defp update_fields([c | rest] = fields, str, index, result) when is_list(fields) do
    if index == 0 do
      update_fields(rest, str, index - 1, [str | result])
    else
      update_fields(rest, str, index - 1, [c | result])
    end
  end

  def update_fields(fields, str, index) when is_bitstring(fields) do
    update_fields(String.to_charlist(fields), str, index, [])
  end

  defp empty_fields() do
    "                         "
  end

  defp starting_fields() do
    Enum.take_random(0..24, 5)
    |> Enum.reduce(empty_fields(), fn x, acc -> update_fields(acc, "X", x) end)
  end

  def check_win_conditions(fields) do
    fields
    |> String.to_charlist()
    |> Enum.reduce(0, fn x, acc -> if [x] == 'K', do: acc + 1, else: acc end) == 5
  end
end
