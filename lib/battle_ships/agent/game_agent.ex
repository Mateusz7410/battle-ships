defmodule BattleShips.GameAgent do
  use Agent
  def start_link(_params) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def put_player(player) do
    Agent.update(__MODULE__, &Map.put_new(&1, player.name, player))
  end


  def get_player(name) do
    Agent.get(__MODULE__, &Map.get(&1, name))
  end


  def update_player(player) do
    Agent.update(__MODULE__, &Map.put(&1, player.name, player))
    player
  end

  def delete_player(name) do
    Agent.update(__MODULE__, &Map.delete(&1, name))
  end

  def get_map() do
    Agent.get(__MODULE__, &Map.keys(&1))
  end
end
