defmodule BattleShips.Repo do
  use Ecto.Repo,
    otp_app: :battle_ships,
    adapter: Ecto.Adapters.Postgres
end
