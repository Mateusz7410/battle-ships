defmodule BattleShips.Player do
  use Ecto.Schema
  import Ecto.Changeset

  schema "players" do
    field(:name)
    field(:is_inviting, :integer)
    field(:is_invited, :integer)
    field(:game_id, :integer)
    timestamps()
  end

  def changeset(player, params \\ :empty) do
    player
    |> cast(params, [:name, :is_inviting, :is_invited, :game_id])
    |> validate_required([:name])
  end

  def create(params) do
    changeset(%BattleShips.Player{}, params)
    |> BattleShips.Repo.insert()
  end

  def update(player, params) do
    changeset(player, params)
    |> BattleShips.Repo.update()
  end

  def delete(player) do
    player
    |> BattleShips.Repo.delete()
  end
end