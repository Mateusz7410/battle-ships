defmodule BattleShips.Game do
  use Ecto.Schema
  import Ecto.Changeset

  schema "games" do
    field(:fields_1)
    field(:fields_2)
    field(:user_1, :integer)
    field(:user_2, :integer)
    timestamps()
  end

  def changeset(game, params \\ :empty) do
    game
    |> cast(params, [:user_1, :user_2, :fields_1, :fields_2])
    |> validate_required([:user_turn, :user_2, :circle])
  end

  def create(params) do
    changeset(%BattleShips.Player{}, params)
    |> BattleShips.Repo.insert()
  end

  def update(game, params) do
    changeset(game, params)
    |> BattleShips.Repo.update()
  end

  def delete(game) do
    game
    |> BattleShips.Repo.delete()
  end
end