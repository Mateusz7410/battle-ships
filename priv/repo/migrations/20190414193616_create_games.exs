defmodule BattleShips.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
  	create table(:games) do
      add(:user_1, :integer, null: false)
      add(:user_2, :integer, null: false)
      add(:fields_1, :text, default: "                         ")
      add(:fields_2, :text, default: "                         ")
      timestamps()
    end
  end
end
