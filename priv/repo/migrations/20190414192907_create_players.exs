defmodule BattleShips.Repo.Migrations.CreatePlayers do
  use Ecto.Migration

   def change do
    create table(:players) do
      add(:name, :text, null: false)
      add(:is_inviting, :integer, default: -1)
      add(:is_invited, :integer, default: -1)
      add(:game_id, :integer, default: -1)
      timestamps()
    end
  end
end
